<?php get_header(); ?>


    <main>

        <div class="container">
                                
                <table class="table__archives" data-filtering="true">
                    <thead>
                        <tr>
                        <th class="club__nom">Nom du club</th>
                        <th class="club__comite">Comité</th>
                        <th class="club__ville">Ville</th>
                        <th class="club__courts">Courts intérieurs</th>
                        <th class="club__courts">Courts extérieurs</th>
                        </tr>
                    </thead>
                    
                    <?php while( have_posts() ): the_post(); ?>
                    
                    <?php
                    
                        $courtsInt = 0;
                        $courtsInt += get_field('resine_interieur');
                        $courtsInt += get_field('beton_interieur');
                        $courtsInt += get_field('tb_interieur');
                        $courtsInt += get_field('moquette_interieur');
                        $courtsInt += get_field('autre_interieur');

                        $courtsExt = 0;
                        $courtsExt += get_field('beton_exterieur');
                        $courtsExt += get_field('tb_exterieur');
                        $courtsExt += get_field('gazon_exterieur');
                    
                    ?>
                
                    <tr class="clickable-row" data-href="<?php the_permalink(); ?>">
                        <td class="club__nom-row"><?php the_field('nom_club'); ?></td>
                        <td class="club__comite-row"><?php the_field('comite_club'); ?></td>
                        <td class="club__comite-ville"><?php the_field('ville_club'); ?></td>
                        <td class="club__courts-row"><?php echo $courtsInt; ?></td>
                        <td class="club__courts-row"><?php echo $courtsExt; ?></td>
                    </tr>
                    
                    <?php endwhile; ?>
                    
            </table>            
            
            </div>

        



    </main>


    <?php get_footer(); ?>
<?php get_header(); ?>

    <div class="container main__wrapper">

        <main class="main__content">

            <section>

                <?php   if ( have_posts() ): while ( have_posts() ): the_post(); 
                                       
                            the_content();
                         
                        endwhile; endif; ?>


            </section>

        </main>

    <aside class="main__sidebar">
            
            <?php if( ($boucleResultats->have_posts()) ): ?>

            <section class="home__results">

                <h2>Résultats</h2>

                <div class="home__results-wrap">
                    
                    <?php while( ($boucleResultats->have_posts()) ): 
                    $boucleResultats->the_post(); ?>

                    <div class="result">

                        <div class="result__type">
                            
                            <?php $idResultat = get_field('tournoi_resultat')[0]; ?>
                            
                            <h3><?php the_field('nom_tournoi', $idResultat); ?></h3>
                            <span class="champ__type">Messieurs (seniors)</span>

                        </div>
                        
                            
                            <?php   // Désignation du vainqueur
                        
                                    $setsJ1 = 0;
                                    $setsJ1 += get_field('joueur1_set1');
                                    $setsJ1 += get_field('joueur1_set2');
                                    $setsJ1 += get_field('joueur1_set3');

                                    $setsJ2 = 0;
                                    $setsJ2 += get_field('joueur2_set1');
                                    $setsJ2 += get_field('joueur2_set2');
                                    $setsJ2 += get_field('joueur2_set3');
                        
                                    $statutJ1 = $statutJ2 = $j1Set1 = $j1Set2 = $j1Set3 = $j2Set1 = $j2Set2 = $j2Set3 = '';
                        
                                    if( $setsJ1 > $setsJ2 ) {
                                        
                                        $statutJ1 = 'winner';
                                        
                                    } else {
                                        
                                        $statutJ2 = 'winner';
                                        
                                    }
                        
                        
                                    if( get_field('joueur1_set1') > get_field('joueur2_set1') ) {
                                        
                                        $j1Set1 = 'win-set';
                                        
                                    } else {
                                        
                                        $j2Set1 = 'win-set';
                                        
                                    }
                        
                                    if( get_field('joueur1_set2') > get_field('joueur2_set2') ) {
                                        
                                        $j1Set2 = 'win-set';
                                        
                                    } else {
                                        
                                        $j2Set2 = 'win-set';
                                        
                                    }
                        
                                    if( get_field('joueur1_set3') > get_field('joueur2_set3') ) {
                                        
                                        $j1Set3 = 'win-set';
                                        
                                    } else {
                                        
                                        $j2Set3 = 'win-set';
                                        
                                    }
                        ?>
                        
                        
                        <div class="result__table">
                            <div class="result__line">
                                <span class="result__player <?php echo $statutJ1; ?>"><?php the_field('joueur1'); ?></span>
                                <span class="result--set <?php echo $j1Set1; ?>"><?php the_field('joueur1_set1'); ?></span>
                                <span class="result--set <?php echo $j1Set2; ?>"><?php the_field('joueur1_set2'); ?></span>
                                <span class="result--set <?php echo $j1Set3; ?>"><?php the_field('joueur1_set3'); ?></span>
                            </div>

                            <div class="result__line">
                                <span class="result__player <?php echo $statutJ2; ?>"><?php the_field('joueur2'); ?></span>
                                <span class="result--set <?php echo $j2Set1; ?>"><?php the_field('joueur2_set1'); ?></span>
                                <span class="result--set <?php echo $j2Set2; ?>"><?php the_field('joueur2_set2'); ?></span>
                                <span class="result--set <?php echo $j2Set3; ?>"><?php the_field('joueur2_set3'); ?></span>
                            </div>
                        </div>

                    </div>
                    
                    <?php endwhile; ?>

                </div>

                <a class="button--blue all-results__button">Tous les résultats</a>

            </section>
            
            <?php endif; wp_reset_postdata(); ?>


            <section class="home__quick-links">

                <h2>Accès rapide</h2>

                <ul class="quick-links">

                        <li><a href="https://aei.app.fft.fr/ei/connexion.do?dispatch=afficher" target="_blank" class="arrow-link">AEI (espace juges-arbitres)</a></li>
                        <li><a href="http://www.gs.applipub-fft.fr/fftfr/frameset.do?dispatch=load" target="_blank" class="arrow-link">Gestion sportive (interclubs)</a></li>
                        <li><a href="https://adoc.app.fft.fr/adoc/" target="_blank" class="arrow-link">ADOC (gestion de club)</a></li>
                        <li><a href="http://www.ligue.fft.fr/franche-comte/commun/agenda/Annexes/calendrier-sportif.pdf" target="_blank" class="arrow-link">Calendrier sportif</a></li>
                        <li><a href="/annonceurs" target="_blank" class="arrow-link">Club des annonceurs</a></li>
                        <li><a href="http://www.fft.fr/sites/default/files/pdf/web_licence_periscolaire_2016.pdf" target="_blank" class="arrow-link">Nouvelle licence FFT périscolaire</a></li>
                        <li><a href="http://www.fft.fr/sites/default/files/pdf/web_licence_decouverte_20161.pdf" target="_blank" class="arrow-link">Nouvelle licence FFT découverte</a></li>

                </ul>

            </section>


    </aside>

    </div>

<?php get_footer(); ?>
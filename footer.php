    <footer>
        
        <div class="container">
            
            <div class="footer__nav-menu">
            
                <?php wp_nav_menu( array('theme_location' => 'menu') ); ?>
            
            </div>
        
            <div class="footer__more">
                
                <div>
                <ul class="footer__menu">

                    <li><a class="footer__top-link" href="<?php echo get_home_url(); ?>/mentions-legales">Mentions légales</a></li>
                    <li><a class="footer__top-link" href="<?php echo get_home_url(); ?>/contact">Contact</a></li>

                </ul>
                
            </div>

                <p>Pour nous contacter, vous pouvez utiliser le <a href="<?php echo get_home_url(); ?>/contact">formulaire de contact</a> ou nous joindre aux coordonnées ci-dessous :</p>

                <p><b>Ligue de Franche-Comté de Tennis</b><br />
                    BP 21251 - 49 Avenue de l 'Observatoire<br />
                    25004 Besançon Cedex 3</p>

                <p>Tél. : 03 81 50 27 18<br />
                    Fax : 03 81 50 77 21</p>
                
                </div>

                    <ul class="footer__logos">
                        <li class="logo-s logo-fft" ><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-fft.png" srcset="<?php echo get_template_directory_uri() ?>/assets/img/svg/logo-fft.svg" alt="Fédération Française de Tennis"></li>
                        <li class="logo-s logo-rg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-rg.png" srcset="<?php echo get_template_directory_uri() ?>/assets/img/svg/logo-rg.svg" alt="Roland Garros"></li>
                        <li class="logo-l logo-cdc"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-cdc.png" srcset="<?php echo get_template_directory_uri() ?>/assets/img/svg/logo-cdc.svg" alt="La Centrale des Clubs"></li>
                        <li class="logo-l logo-nrg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-nrg.png" srcset="<?php echo get_template_directory_uri() ?>/assets/img/svg/logo-nrg.svg" alt="Nouveau Roland Garros"></li>
                    </ul>
                
            
        </div>
        
    </footer>


<?php wp_footer() ?>

</body>
</html>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <nav class="nav">

        <div class="container nav__navbar">

            <a href="<?php echo get_home_url(); ?>" class="navbar__logo"></a>

            <div class="navbar__menu">
                <div class="menu__icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>

                <span class="menu__text">MENU</span>
            </div>

            <div class="nav__menu">
                <div class="container">
                    <?php wp_nav_menu( array('theme_location' => 'menu') ); ?>
                </div>
            </div>
        </div>

    </nav>

    <?php   $argsEpingle = array(
            
                'post_type' => 'post',
                'category_name' => 'article_epingle',
                'posts_per_page' => 1,
                'order' => 'DESC',
                'orderby' => 'date'
                
            );
    
            $boucleEpingle = new WP_Query($argsEpingle);
    
            if ( is_front_page() ): 
    
    
    ?>  
    
        <?php if( $boucleEpingle->have_posts() ): while( $boucleEpingle->have_posts() ): $boucleEpingle->the_post(); ?>

        <header class="header__home" style="background-image: url(<?php the_field('fond_actualite'); ?>); background-position: <?php the_field('alignement_bg') ?> center;">

            <div class="header__overlay"></div>

            <div class="container"><span><?php echo $debug; ?></span>
                <div class="header__hero">

                    <div class="hero__logo-tennisfc"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-tennisfc.png" srcset="<?php echo get_template_directory_uri() ?>/assets/img/svg/logo-tennisfc.svg" width="235" height="111" alt="Ligue de Franche-Comté de Tennis"></div>

                    <div class="hero__news">

                        <h2><?php the_title(); ?></h2>
                        <p><?php the_field('contenu_article'); ?></p>

                    </div>

                    <?php   if( have_rows('boutons_actualite') ): while( have_rows('boutons_actualite') ): the_row(); 
                        
                                    if( get_sub_field('url_lien') ) {

                                        $lien = get_sub_field('url_lien');

                                    } else if( get_sub_field('dl_lien') ) {

                                        $lien = get_sub_field('dl_lien');

                                    }
                    ?>
                    
                        <a class="hero__button" href="<?php echo $lien; ?>"><?php the_sub_field('titre_lien'); ?></a>
                    
                    <?php endwhile; endif; ?>
                    
                    <?php endwhile; endif; wp_reset_postdata(); ?>
                </div>
            </div>

        </header>

        <section class="header__blocks">
            <div class="container">
                <h1 class="header__pre-blocks">Avec la Ligue de Franche-Comté de Tennis , il y a forcément un tennis qui vous ressemble</h1>

                <div class="header__blocks-wrap">
                    <div class="block__club">

                        <span class="number">138</span>
                        <span class="type">clubs</span>
                        <a href="/clubs"><button class="button--white button--block__club">Trouvez
                            <br />votre club</button></a>

                    </div>

                    <div class="block__club__enseignant">

                        <span class="number">192</span>
                        <span class="type">tournois</span>
                        <a href="/tournois"><button class="button--white button--block__club">Trouvez
                            <br />votre tournoi</button></a>

                    </div>

                    <div class="block__club__tournoi">

                        <span class="number">138</span>
                        <span class="type">enseignants</span>
                        <a href="/enseignants"><button class="button--white button--block__club">Trouvez
                            <br />votre enseignant</button></a>

                    </div>
                </div>
            </div>

        </section>

        <?php else: ?>

            <header class="page-title">
                
                <div class="container">
                
                    <i class="lnr lnr-magnifier"></i>
                    
                    <?php if( is_singular('clubs') ): ?>
                    
                        <h1>Fiche club</h1>
                    
                    <?php elseif( is_singular('tournois') ): ?>
                    
                        <h1>Fiche tournoi</h1>
                    
                    <?php else: ?>
                    
                        <h1><?php wp_title(''); ?></h1>
                    
                    <?php endif; ?>
                    
                </div>
                
            </header>
    
        <?php endif; ?>
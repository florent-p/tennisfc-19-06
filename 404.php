<?php get_header(); ?>


<main>

    <div class="container">
        
        <h2>La page que vous essayez de consulter n'existe pas.</h2>
        <p>En cas de problème, n'hésitez pas à nous <a href="<?php echo get_home_url(); ?>/contact">contacter</a>.</p><br />
        <a class="button--blue" href="<?php echo get_home_url(); ?>">Retour à l'accueil</a>

        
    </div>
        
</main>

<?php get_footer(); ?>
<?php

function scripts_js() {
    
    if ( is_page_template('templates/recherche-club.php') ) {
        wp_enqueue_script('map-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDzP9o4zX9Me8Z0Mlrrl-nnriLtfgyw2_w&libraries=places', false, '', true);
        wp_enqueue_script('geo', 'https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js', array ( 'jquery' ), '', true);
        wp_enqueue_script('map', get_template_directory_uri() . '/assets/js/map.js', array ( 'jquery' ), '', true);
        
    }
    
    if ( is_singular('clubs') ) {
        wp_enqueue_script('map', get_template_directory_uri() . '/assets/js/map-single.js', array ( 'jquery' ), '', true);
        wp_enqueue_script('map-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDzP9o4zX9Me8Z0Mlrrl-nnriLtfgyw2_w&callback=initMap', false, '', true);
        wp_enqueue_script('geo', 'https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js', array ( 'jquery' ), '', true);
        
    }
    
    if ( is_post_type_archive() ) {
        wp_enqueue_style('dt-css', get_template_directory_uri() . '/assets/css/dataTables.min.css', false, '', all);
        wp_enqueue_script('dt-js', get_template_directory_uri() . '/assets/js/dataTables.js', array ( 'jquery' ), '', true);
    }
    
    // Custom
    wp_enqueue_script('js', get_template_directory_uri() . '/assets/js/tennisfc.js', array ( 'jquery' ), '', true);
    
}

function styles_css() {
	// Style CSS
    wp_enqueue_style('icons', 'https://file.myfontastic.com/Ng3a6bpLj94WyrxT2g3RMm/icons.css', false, '', all);
    wp_enqueue_style('css', get_template_directory_uri() . '/assets/css/main.css', false, '', all);
}

    add_action( 'wp_enqueue_scripts', 'scripts_js' );
    add_action( 'wp_enqueue_scripts', 'styles_css' );


// Position menu header
register_nav_menus( array(
	'menu' => 'Menu',
) );

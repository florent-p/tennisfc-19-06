<?php get_header(); ?>


    <main>

        <div class="container">
                                
                <table class="table__archives" data-filtering="true">
                    <thead>
                        <tr>
                        <th class="tournoi__nom">Nom du tournoi</th>
                        <th class="tournoi__club">Organisé par</th>
                        <th class="tournoi__ville">Ville</th>
                        <th class="tournoi__date">Début</th>
                        <th class="tournoi__date">Fin</th>
                        </tr>
                    </thead>
                    
                    <?php while( have_posts() ): the_post(); ?>
                    
                    
                                
                    <tr class="clickable-row" data-href="<?php the_permalink(); ?>">
                        <td class="tournoi__nom-row"><?php the_field('nom_tournoi'); ?></td>
                        
                        <?php   $clubOrg = get_field('club_organisateur');
                                $dateD = get_field('debut_tournoi', false, false);
                                $dateF = get_field('fin_tournoi', false, false);

                                $dateDebut = new DateTime($dateD);
                                $dateFin = new DateTime($dateF);

                                
                                if( $clubOrg ):
                        
                                foreach( $clubOrg as $post ): 
                                setup_postdata($post); 
                        ?>                   
                        
                                    <td class="tournoi__club-row"><?php the_field('nom_club'); ?></td>
                        
                        <?php endforeach; endif; wp_reset_postdata(); ?>
                        
                        <td class="tournoi__ville-row"><?php the_field('ville_tournoi'); ?></td>
                        <td class="tournoi__date-row"><?php echo $dateDebut->format('d/m/Y'); ?></td>
                        <td class="tournoi__date-row"><?php echo $dateFin->format('d/m/Y'); ?></td>
                    </tr>
                    
                    <?php endwhile; ?>
                    
            </table>            
            
            </div>

        



    </main>


    <?php get_footer(); ?>
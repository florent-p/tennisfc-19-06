(function($) {
var popup = document.getElementById('popupClub');
var geocoder = new google.maps.Geocoder();

   
    
function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 8,
		center		: {lat: 46.885181, lng: 5.890408},
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	}; 
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
    
    
    // Autocomplete
    var options = {
       types: ['(regions)'],
       componentRestrictions: {country: 'fr'}
        
    };
    
    var input = document.getElementById('search-club');
    autocomplete = new google.maps.places.Autocomplete(input, options);
    
    
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
    var place = autocomplete.getPlace();
    map.setCenter(place.geometry.location);
    map.setZoom(10);
    });

    
    
/* var options = {
   types: ['(cities)'],
   componentRestrictions: {country: 'fr'}
};
var input = document.getElementById('search-input');
autocomplete = new google.maps.places.Autocomplete(input, options); */


	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});

	
	// return
	return map;
	
}

    function add_marker( $marker, map ) {

        var adresse = $marker.attr('data-addr');
        geocoder.geocode( { 'address' : adresse }, function(results, status) {
            
            var latClub = results[0].geometry.location.lat();
            var lngClub = results[0].geometry.location.lng();
            
            var latLng = new google.maps.LatLng( latClub, lngClub );
            
            // Passe la variable latLng dans la fonction coordonnées
            coordonnees(latLng, latClub, lngClub);
        });


        // Fonction avec la variable latLng passée ci-dessus utilisée en tant que latLngMarker
        function coordonnees(latLngMarker, centerClubLat, centerClubLng) {

        // create marker
        var icon = 'http://i.imgur.com/zEm9OZ2.png';
        var marker = new google.maps.Marker({
            position	: latLngMarker,
            map			: map,
            icon: icon
        });
    
            
	map.markers.push( marker );

            /*/ create info window
            var infowindow = new google.maps.InfoWindow({
                content		: $marker.html()
            }); */
            
            // show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', function() {
                
                map.setCenter ({
                   lat: centerClubLat,
                    lng: centerClubLng
                    
                });
                
                //Réinitialise l'ensemble des icônes à leur état normal
                for (var i=0; i<map.markers.length; i++) {
                    map.markers[i].setIcon('http://i.imgur.com/zEm9OZ2.png');
                }
                
                // Remplace l'icône sélectionnée
                marker.setIcon('http://i.imgur.com/t1e3rlo.png')
                
                var markerHtml = $marker.html();
                popup.classList.add('active');
                popup.innerHTML = markerHtml;
                
                
                            
                    document.getElementById('close__popupClub').addEventListener('click', function() {

                    popup.classList.remove('active');

                });
                

                //infoB.open();

            });

    }

}



var map = null;

$(document).ready(function(){


		// create map
		map = new_map( $("#map") );

});

})(jQuery);
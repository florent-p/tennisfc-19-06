jQuery(document).ready(function($) {
    var jq = $.noConflict();
    
    // [Menu] Suppression de l'attribut href sur les titres de menu ("La ligue", ...)
    jq('.sub__cat').children('a.no-href').removeAttr('href');
    
    
    jq('.navbar__menu', this).click(function() {
        
        // [Menu] Comportement de l'icône menu et affichage du menu
        jq(this).toggleClass('menu--active');
        
        var isMenuVisible = jq('.nav__menu').is(":visible");
        
        if (isMenuVisible) {
            
            jq('.nav__menu').stop().slideUp();
            
        } else {
            
            jq('.nav__menu').stop().slideDown();
        }
        
        // ANIMATION ICONE BURGER
        jq('.menu__icon').toggleClass('icon--active');
    
        
    });
    
    // [Menu] Comportement du menu sur mobile
    function mobileMenu() {

        jq('.sub__cat').children('a').on('click', function() {
            
            var subLinks = jq(this).parent().find('.menu__subtitle');
            
            if ( jq(this).hasClass('cat--opened') ) {
                
                subLinks.stop().fadeOut();
                jq(this).removeClass('cat--opened');
                
            } else {
                
                subLinks.stop().fadeIn();
                jq(this).addClass('cat--opened');

            }
            
            

        });
    }
    
    if ( jq(window).width() < 1366 ) {
        
        jq('.sub__cat').find('.menu__subtitle').hide();
        jq('.sub__cat').children('a').removeClass('cat--opened');
        mobileMenu();
        
    }
    
    // [Menu] Ajoute un wrapper pour faciliter la mise en forme du menu et affiche tous les éléments du menu sur Desktop
    function desktopMenu() {
        
        if ( jq(window).width() > 1365 ) {
            
            jq('.sub__cat').find('.menu__subtitle').show();
            
            if ( jq('nav .elements__right').length > 0 ) {

            } else {
                
                jq('nav .el--right').wrapAll('<div class="elements__right"> </div>');
                
            }
            
        } 
        
        
        if ( jq(window).width() > 767 ) {
            
            if ( jq('footer .elements__right').length > 0 ) {

            } else {
                
                jq('footer .el--right').wrapAll('<div class="elements__right"> </div>');
                
            }
            
        }
        
        
    }
    
    
    
    
    
    // Page recherche de club (carte) : input de recherche (.map__search) au-dessus de la map en Mobile & Tablette / sur la carte en Desktop
    function mapSearchBox() {
        
        if ( jq('body').hasClass('page-template-recherche-club') && jq(window).width() > 1365 ) {
            
            // Détache la div .map__search et la déplace dans #map
            jq('.map__search').detach().appendTo('.map__wrapper');
            
        } else {
            
            // Détache la div .map__search et la déplace dans .map__wrapper avant #map
            jq('.map__search').detach().appendTo('.map__page').insertBefore('.map__wrapper');
            
        }
        
    }
    
    
        // Page archives (tableaux)
    function hideRows() {
        
        if ( jq('body').hasClass('archive') && jq(window).width() < 767 ) {
            
            // Masque les colonnes superflues
            jq('.club__comite').hide();
            jq('.club__courts').hide();
            jq('.club__comite-row').hide();
            jq('.club__courts-row').hide();
            
            jq('.ens__clubs').hide();
            jq('.ens__clubs-row').hide();
            
            jq('.tournoi__club').hide();
            jq('.tournoi__club-row').hide();
            jq('.tournoi__date').hide();
            jq('.tournoi__date-row').hide();
            
            
        } else {
            
            // Affiche les colonnes 
            jq('.club__comite').show();
            jq('.club__courts').show();
            jq('.club__comite-row').show();
            jq('.club__courts-row').show();
            
            jq('.ens__clubs').show();
            jq('.ens__clubs-row').show();
            
            jq('.tournoi__club').show();
            jq('.tournoi__club-row').show();
            jq('.tournoi__date').show();
            jq('.tournoi__date-row').show();
            
        }
        
    }
    
    
    
    
    // Ligne entièrement cliquable (tableaux)
    
    jq('.clickable-row').click(function() {
       
        window.location = jq(this).attr('data-href');
        
    });
    
    // Filtres tableaux
    
    if ( jq('body').hasClass('archive') ) {
        
        hideRows();
        jq('.table__archives').DataTable();
        
    }
    
    
    // Appelle les fonctions mobileMenu, mapSearchBox et hideRows en cas de redimensionnement
    jq(window).resize( function() {
        jq('.sub__cat').find('.menu__subtitle').hide();
        jq('.sub__cat').children('a').removeClass('cat--opened');
        desktopMenu();
        mobileMenu();
        mapSearchBox();
        hideRows();
        
    });
    
    // Appelle les fonctions au chargement de la page
    mapSearchBox();
    desktopMenu();
    hideRows();
    
    
    // Supprime les <p> vides
    jq('p:empty').remove();
    
    
});



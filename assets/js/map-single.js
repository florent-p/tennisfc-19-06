var map;



          function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            disableDefaultUI: true

        });
      
        var geocoder = new google.maps.Geocoder();
        var adresse = document.getElementById('address');
        geocoder.geocode( { 'address' : adresse.dataset.addr }, function(results, status) {
            
            var latClub = results[0].geometry.location.lat();
            var lngClub = results[0].geometry.location.lng();
            
            var latLng = new google.maps.LatLng( latClub, lngClub );
            
            // Passe la variable latLng dans la fonction coordonnées
            coordonnees(latLng, latClub, lngClub);
        });


        // Fonction avec la variable latLng passée ci-dessus utilisée en tant que latLngMarker
        function coordonnees(latLngMarker, centerClubLat, centerClubLng) {

        map.setCenter(latLngMarker);
        // create marker
        var icon = 'http://i.imgur.com/zEm9OZ2.png';
        var marker = new google.maps.Marker({
            position	: latLngMarker,
            map			: map,
            icon: icon
        });
        }


       }

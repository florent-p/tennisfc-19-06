<?php get_header(); 

$resultatRel = get_posts(array(
                        'post_type' => 'resultats',
                        'meta_query' => array(
                            array(
                                'key' => 'tournoi_resultat', 
                                'value' => '"' . get_the_ID() . '"', 
                                'compare' => 'LIKE'
                            )
                        )
                    ));

?>

    <main>

        <div class="container">
            
            <h2><?php wp_title(); ?></h2>
            
            <div class="champ">
            
                <div class="champ__infos">

                    
                    
                    <h3>Le tournoi</h3>

                    <?php   $clubOrg = get_field('club_organisateur');
                            if( $clubOrg ): ?>
                    
                            <h4>Organisé par</h4>
                    
                                <?php foreach( $clubOrg as $post ): 
                                    setup_postdata($post); ?>

                    <span><a href="<?php the_permalink(); ?>"><?php the_field('nom_club'); ?></a></span>

                    <?php endforeach; endif; wp_reset_postdata; ?>
                    
                    <?php while( have_posts() ): the_post(); ?>

                        <h4>Dates</h4>
                        <span>du <?php the_field('debut_tournoi'); ?> au <?php the_field('fin_tournoi'); ?></span>
                    
                        <h4>Lieu</h4>
                        <span><?php the_field('adresse_tournoi'); ?> - <?php the_field('cp_tournoi'); ?> <?php the_field('ville_tournoi'); ?></span>
                    
                        <h4>Juge-arbitre</h4>
                        <span><?php the_field('jat_tournoi'); ?></span>
                    
                        <h4>Montant des lots</h4>
                        <span><?php the_field('lots_tournoi'); ?> €</span>
                    
                    <?php endwhile; ?>
                        


                </div>
            
                <div class="champ__details">
                    
                    <h3>Inscription</h3>
                    
                    <h4>Responsable :</h4>
                    <span><?php the_field('responsable_tournoi'); ?></span>
                    
                    <h4>Adresse :</h4>
                    <span><?php the_field('adresse1_responsable'); ?> - <?php the_field('adresse2_responsable'); ?></span>
                    
                    <h4>Téléphone :</h4>
                    <span><?php the_field('tel_responsable'); ?></span>
                    
                    <h4>Adresse email :</h4>
                    <span><?php the_field('email_responsable'); ?></span>
                    
                </div>
              
            </div>
            
            
            
            <?php if( $resultatRel ): ?>
                <div class="champ">
                    
                    <div class="champ__results">
                        
                    <h3>Résultats</h3>
            
                            
                            <div class="results">

                                <?php foreach( $resultatRel as $resultat ): ?>

                                <div class="result">
                                    

                                <?php   // Désignation du vainqueur

                                        $setsJ1 = 0;
                                        $setsJ1 += get_field('joueur1_set1', $resultat->ID);
                                        $setsJ1 += get_field('joueur1_set2', $resultat->ID);
                                        $setsJ1 += get_field('joueur1_set3', $resultat->ID);

                                        $setsJ2 = 0;
                                        $setsJ2 += get_field('joueur2_set1', $resultat->ID);
                                        $setsJ2 += get_field('joueur2_set2', $resultat->ID);
                                        $setsJ2 += get_field('joueur2_set3', $resultat->ID);

                                        $statutJ1 = $statutJ2 = $j1Set1 = $j1Set2 = $j1Set3 = $j2Set1 = $j2Set2 = $j2Set3 = '';

                                        if( $setsJ1 > $setsJ2 ) {

                                            $statutJ1 = 'winner';

                                        } else {

                                            $statutJ2 = 'winner';

                                        }


                                        if( get_field('joueur1_set1', $resultat->ID) > get_field('joueur2_set1', $resultat->ID) ) {

                                            $j1Set1 = 'win-set';

                                        } else {

                                            $j2Set1 = 'win-set';

                                        }

                                        if( get_field('joueur1_set2', $resultat->ID) > get_field('joueur2_set2', $resultat->ID) ) {

                                            $j1Set2 = 'win-set';

                                        } else {

                                            $j2Set2 = 'win-set';

                                        }

                                        if( get_field('joueur1_set3', $resultat->ID) > get_field('joueur2_set3', $resultat->ID) ) {

                                            $j1Set3 = 'win-set';

                                        } else {

                                            $j2Set3 = 'win-set';

                                        }
                            ?>


                            <div class="result__table">
                                <div class="result__line">
                                    <span class="result__player <?php echo $statutJ1; ?>"><?php the_field('joueur1', $resultat->ID); ?></span>
                                    <span class="result--set <?php echo $j1Set1; ?>"><?php the_field('joueur1_set1', $resultat->ID); ?></span>
                                    <span class="result--set <?php echo $j1Set2; ?>"><?php the_field('joueur1_set2', $resultat->ID); ?></span>
                                    <span class="result--set <?php echo $j1Set3; ?>"><?php the_field('joueur1_set3', $resultat->ID); ?></span>
                                </div>

                                <div class="result__line">
                                    <span class="result__player <?php echo $statutJ2; ?>"><?php the_field('joueur2', $resultat->ID); ?></span>
                                    <span class="result--set <?php echo $j2Set1; ?>"><?php the_field('joueur2_set1', $resultat->ID); ?></span>
                                    <span class="result--set <?php echo $j2Set2; ?>"><?php the_field('joueur2_set2', $resultat->ID); ?></span>
                                    <span class="result--set <?php echo $j2Set3; ?>"><?php the_field('joueur2_set3', $resultat->ID); ?></span>
                                </div>
                            </div>

                        </div>   


                                <?php endforeach; ?>
                        </div>
                           
                </div>
            </div>

            <?php endif; ?>
            
            </div>
            
                

    </main>


    <?php get_footer(); ?>
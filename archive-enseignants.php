<?php get_header(); ?>


    <main>

        <div class="container">
                                
                <table class="table__archives" data-filtering="true">
                    <thead>
                        <tr>
                        <th class="ens__nom">Nom</th>
                        <th class="ens__prenom">Prénom</th>
                        <th class="ens__diplome">Diplôme</th>
                        <th class="ens__clubs">Enseigne à</th>
                        </tr>
                    </thead>
                    
                    <?php while( have_posts() ): the_post(); ?>
                    
                    
                                
                    <tr>
                        <td class="ens__nom-row"><?php the_field('nom_enseignant'); ?></td>
                        <td class="ens__prenom-row"><?php the_field('prenom_enseignant'); ?></td>
                        <td class="ens__diplome-row"><?php the_field('diplome_enseignant'); ?></td>
                        
                        <td class="ens__clubs-row">
                            <?php   $clubEns = get_field('clubs_enseignant');
                                
                                if( $clubEns ):
                        
                                foreach( $clubEns as $post ): 
                                setup_postdata($post); 
                            ?>                   
                        
                            <span><?php the_field('nom_club'); ?></span>
                            
                            <?php endforeach; endif; wp_reset_postdata(); ?>
                            
                        </td>

                    </tr>
                    
                    <?php endwhile; ?>
                    
            </table>            
            
            </div>

        



    </main>


    <?php get_footer(); ?>
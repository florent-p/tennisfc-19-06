<?php 

/*
Template Name: Recherche club (carte)
*/

get_header();

$args = array(
    'post_type' => 'clubs',
    'post_status' => 'publish'
    );

$boucleClubs = new WP_Query( $args );

?>

    <main>

        <div class="container map__page">
   
            <div class="map__search">
            <h2 class="search__title">Trouver un club près de :</h2>
                <input id="search-club" class="search__input" type="text">
                <p>Sinon :</p>
                <a href="#" class="arrow-link">Effectuer une recherche multi-critères</a>
            </div>
                
                <div class="map__wrapper">
                <div id="popupClub" style="z-index: 9999;"></div>
                    
                    <div id="map">


                        <?php if( $boucleClubs->have_posts() ): while( $boucleClubs->have_posts() ): $boucleClubs->the_post(); ?>

                    <div class="marker" data-addr="<?php the_field('adresse_club').' '.the_field('cp_club').' '.the_field('ville_club'); ?>">

                        <div id="close__popupClub"><i class="icon-close"></i></div>                  
                        <span class="popupClub__club">Club sélectionné :</span>
                        <span class="popupClub__selected"><?php the_field('nom_club'); ?></span>
                        <span class="popupClub__info--adress">
                            
                            <?php if( get_field('adresse_club') ): ?>
                            
                                <?php the_field('adresse_club'); ?><br />
                            
                            <?php endif; ?>
                            
                            <?php the_field('cp_club'); ?> <?php the_field('ville_club'); ?>
                            
                        </span>
                        <span class="popupClub__info--tel"><?php the_field('tel_club'); ?></span>
                        <span class="popupClub__info--mail"><?php the_field('email_club'); ?></span>
                        
                        <?php if( get_field('web_club') ): ?>
                        
                            <span class="popupClub__info--website"><a href="<?php the_field('web_club'); ?>" target="_blank">Site web</a></span>
                        
                        <?php endif; ?>
                        
                        <a class="button--white button__club" href="<?php the_permalink(); ?>">Fiche complète</a>



                    </div>

                    <?php endwhile; endif; wp_reset_postdata(); ?>

                </div>
                
            </div>
        </div>


        



    </main>


    <?php get_footer(); ?>
<?php get_header(); 

$tournoisRel = get_posts(array(
                        'post_type' => 'tournois',
                        'meta_query' => array(
                            array(
                                'key' => 'club_organisateur', 
                                'value' => '"' . get_the_ID() . '"', 
                                'compare' => 'LIKE'
                            )
                        )
                    ));


$encadrementRel = get_posts(array(
                        'post_type' => 'enseignants',
                        'meta_query' => array(
                            array(
                                'key' => 'clubs_enseignant', 
                                'value' => '"' . get_the_ID() . '"', 
                                'compare' => 'LIKE'
                            )
                        )
                    ));

?>


    <main>

        <div class="container">
                    
                <h2><?php the_field('nom_club'); ?></h2>
                    <div class="club__header"> 
                            <?php while( have_posts() ): the_post(); ?>

                        <div class="club__infos">

                            <input type="hidden" id="address" data-addr="<?php the_field('adresse_club').' '.the_field('cp_club').' '.the_field('ville_club'); ?>">

                            <span class="popupClub__info--adress">

                                <?php if( get_field('adresse_club') ): ?>

                                    <?php the_field('adresse_club'); ?><br />

                                <?php endif; ?>

                                <?php the_field('cp_club'); ?> <?php the_field('ville_club'); ?>

                            </span>
                            <span class="popupClub__info--tel"><?php the_field('tel_club'); ?></span>
                            <span class="popupClub__info--mail"><?php the_field('email_club'); ?></span>

                            <?php if( get_field('web_club') ): ?>

                                <span class="popupClub__info--website"><a href="<?php the_field('web_club'); ?>" target="_blank">Site web</a></span>

                            <?php endif; ?>
                            
                            <?php if( $tournoisRel ): 
                                    
                                    $nbrTournois = count($tournoisRel);
                                    if( $nbrTournois == 1): 
                            ?>
                            
                                <span class="popupClub__info--champ">Tournoi organisé :<br />
                                    
                            <?php elseif( $nbrTournois > 1 ): ?>
                                    
                                <span class="popupClub__info--champ">Tournois organisés :<br />
                            
                            <?php endif; ?>

                                <?php foreach( $tournoisRel as $tournoi ): ?>

                                    <a href="<?php the_permalink($tournoi->ID); ?>"><?php echo $tournoi->post_title; ?></a>


                                <?php endforeach; ?>
                                    
                                </span>
                            
						      <?php endif; ?>

                        </div>

                        <?php endwhile; ?>

                    <div id="map"></div>
                </div>
            
            <div class="club__all-infos">

                <div class="club__infrastructures"><h3>Infrastructures</h3>

                        <?php   $courtsInt = 0;
                                $courtsInt += get_field('resine_interieur');
                                $courtsInt += get_field('beton_interieur');
                                $courtsInt += get_field('tb_interieur');
                                $courtsInt += get_field('moquette_interieur');
                                $courtsInt += get_field('autre_interieur');

                                $courtsExt = 0;
                                $courtsExt += get_field('beton_exterieur');
                                $courtsExt += get_field('tb_exterieur');
                                $courtsExt += get_field('gazon_exterieur');
                        ?>


                        <?php if( $courtsInt > 0 ): ?>
                            <div class="courts">
                                <?php if( $courtsInt == 1): ?>
                                    <span class="courts__count" data-courts="<?php echo $courtsInt; ?>"> court intérieur</span>
                                <?php else: ?>
                                    <span class="courts__count" data-courts="<?php echo $courtsInt; ?>"> courts intérieurs</span>
                                <?php endif; ?>

                                <ul class="courts__detail">

                                    <?php if( get_field('resine_interieur') == 1): ?>
                                        <li data-courts="<?php the_field('resine_interieur'); ?>"> court en résine</li>
                                    <?php endif; ?>
                                        <?php if( get_field('resine_interieur') > 1): ?>
                                            <li data-courts="<?php the_field('resine_interieur'); ?>"> courts en résine</li>
                                        <?php endif; ?>

                                    <?php if( get_field('beton_interieur') == 1): ?>
                                        <li data-courts="<?php the_field('beton_interieur'); ?>"> court en béton</li>
                                    <?php endif; ?>
                                        <?php if( get_field('beton_interieur') > 1): ?>
                                            <li data-courts="<?php the_field('beton_interieur'); ?>"> courts en béton</li>
                                        <?php endif; ?>

                                    <?php if( get_field('tb_interieur') == 1): ?>
                                        <li data-courts="<?php the_field('tb_interieur'); ?>"> court en terre battue</li>
                                    <?php endif; ?>
                                        <?php if( get_field('tb_interieur') > 1): ?>
                                            <li data-courts="<?php the_field('tb_interieur'); ?>"> courts en terre battue</li>
                                        <?php endif; ?>

                                    <?php if( get_field('moquette_interieur') == 1): ?>
                                        <li data-courts="<?php the_field('moquette_interieur'); ?>"> court en moquette</li>
                                    <?php endif; ?>
                                        <?php if( get_field('moquette_interieur') > 1): ?>
                                            <li data-courts="<?php the_field('moquette_interieur'); ?>"> courts en moquette</li>
                                        <?php endif; ?>

                                    <?php if( get_field('autre_interieur') == 1): ?>
                                        <li data-courts="<?php the_field('autre_interieur'); ?>"> autre court (gymnase, parquet, ...)</li>
                                    <?php endif; ?>
                                        <?php if( get_field('autre_interieur') > 1): ?>
                                            <li data-courts="<?php the_field('autre_interieur'); ?>"> autres courts (gymnase, parquet, ...)</li>
                                        <?php endif; ?>

                                </ul>
                            </div>

                        <?php endif; ?>

                        <?php if( $courtsExt > 0 ): ?>
                            <div class="courts">
                                <?php if( $courtsExt == 1): ?>
                                    <span class="courts__count" data-courts="<?php echo $courtsExt; ?>"> court extérieur</span>
                                <?php else: ?>
                                    <span class="courts__count" data-courts="<?php echo $courtsExt; ?>"> courts extérieurs</span>
                                <?php endif; ?>

                                <ul class="courts__detail">

                                    <?php if( get_field('beton_exterieur') == 1): ?>
                                        <li data-courts="<?php the_field('beton_exterieur'); ?>"> court en béton</li>
                                    <?php endif; ?>
                                        <?php if( get_field('beton_exterieur') > 1): ?>
                                            <li data-courts="<?php the_field('beton_exterieur'); ?>"> courts en béton</li>
                                        <?php endif; ?>

                                    <?php if( get_field('tb_exterieur') == 1): ?>
                                        <li data-courts="<?php the_field('tb_exterieur'); ?>"> court en terre battue</li>
                                    <?php endif; ?>
                                        <?php if( get_field('tb_exterieur') > 1): ?>
                                            <li data-courts="<?php the_field('tb_exterieur'); ?>"> courts en terre battue</li>
                                        <?php endif; ?>

                                    <?php if( get_field('gazon_exterieur') == 1): ?>
                                        <li data-courts="<?php the_field('gazon_exterieur'); ?>"> court en gazon</li>
                                    <?php endif; ?>
                                        <?php if( get_field('gazon_exterieur') > 1): ?>
                                            <li data-courts="<?php the_field('gazon_exterieur'); ?>"> courts en gazon</li>
                                        <?php endif; ?>

                                </ul>
                            </div>
                        <?php endif; ?>

                </div>
                
                <div class="club__general"><h3>Le club</h3>
                    <ul class="general__infos">
                        <li><b>Comité : </b><?php the_field('comite_club'); ?></li>
                        <li><b>Président : </b><?php the_field('president_club'); ?></li>
                        <?php if( get_field('correspondant_club') ): ?>
                            <li><b>Correspondant : </b><?php the_field('correspondant_club'); ?></li>
                        <?php endif; ?>
                        <li><b>Club house : </b>
                            <?php $clubHouse = get_field('club_house');            
                                if( $clubHouse == 1 ) {
                                    echo 'Oui';
                                } else {
                                    echo 'Non';
                                } ?>

                        </li>
                    </ul>
                </div>
                
               
                        <?php if( $encadrementRel ): ?>
                
                            <div class="club__encadrants"><h3>Encadrement</h3>
                            

                                <?php foreach( $encadrementRel as $encadrant ): ?>

                                <span><?php echo get_the_title($encadrant->ID); ?> (<b><?php the_field('diplome_enseignant', $encadrant->ID); ?></b>)</span>    


                                <?php endforeach; ?>
                                    
                            </div>
                            
						  <?php endif; ?>
            </div>

        



    </main>


    <?php get_footer(); ?>